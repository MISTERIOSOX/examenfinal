
public class Proyemp {
    
    private int dniemp;
    private int idpro;

    public Proyemp() {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.dniemp;
        hash = 59 * hash + this.idpro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proyemp other = (Proyemp) obj;
        if (this.dniemp != other.dniemp) {
            return false;
        }
        if (this.idpro != other.idpro) {
            return false;
        }
        return true;
    }

   

    public int getDniemp() {
        return dniemp;
    }

    public void setDniemp(int dniemp) {
        this.dniemp = dniemp;
    }

    public int getIdpro() {
        return idpro;
    }

    public void setIdpro(int idpro) {
        this.idpro = idpro;
    }

    
    
}
